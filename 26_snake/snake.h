#ifndef __SNAKE_H_
#define __SNAKE_H_

#include <stdlib.h>
#include <time.h>


#define AMAX 0x200
#define POSIB 4

extern const struct TVector velocidades[POSIB];

struct TVector {
  double x;
  double y;
};

struct TAnillo {
  struct TVector pos;
  struct TVector vel;
};

struct TSnake {
  struct TAnillo anillo [AMAX];
  int cima;
  int vidas;
};

#ifdef _cplusplus
extern "C" {
#endif
  void iniciar    ( int lines, int cols    );
  void parir      ( struct TSnake  *snake  );
  void mover      ( struct TSnake  *snake  );
  void crecer     ( struct TSnake  *snake  );
#ifdef _cplusplus
}
#endif

#endif
