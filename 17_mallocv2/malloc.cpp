#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>


#define N 0x100
#define OUT "texto.txt"
/* Función punto de entrada*/

int main(int argc, char *argv[]){
  char * texto = (char *) malloc(N);
  FILE *pf;

  printf("Dime la frase: ");
  fgets(texto, N, stdin);

  for(int i=0; i<N; i++)
  texto[i] = tolower(texto[i]);

  if (!(pf = fopen(OUT, "w+")) ){
    return EXIT_FAILURE;
  }
  fwrite(texto, sizeof(char), N, pf);
  fread(texto, sizeof(char), N, pf);

  fclose(pf);

  return EXIT_SUCCESS;
}
