#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 0x100

int main(int argc, char *argv[]){

  char **lista;
  char opcion;
  char buffer[N];
  int len;
  int i = 0;
  int p;

  for (p=0; strcmp(buffer, ".\n"); p++){
      lista = (char **) realloc (lista, (p+1) * sizeof(char*));
      printf("Palabra: ");
      fgets(buffer, N-1, stdin); //lee los espacios y los intros
      //scanf(" %s", buffer); no lee los espacios
      //printf("%i", strlen(buffer)); nos dice la longitud de buffer
      len = strlen (buffer);
      lista[p] = (char *) malloc (len);
      strncpy (lista[p], buffer, len);
      lista[p][len-1] = '\0';
  }

  free (lista[p -1]);
  lista[p-1]= NULL;

  for (int l=0; lista[l]; l++)
      printf("%s\n", lista[l]);
  for (int l=0; lista[l]; l++)
      free(lista[l]);

  free(lista);

  return EXIT_SUCCESS;
}
