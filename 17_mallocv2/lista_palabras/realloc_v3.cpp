#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 0x100

int main(int argc, char *argv[]){

  char **lista;
  char opcion;
  char buffer[N];
  int len;
  int i = 0;
  int p = 1;

  for (int p=0; p<2; p++){
      lista = (char **) realloc (lista, p+1 * sizeof(char*));
      printf("Palabra: ");
      fgets(buffer, N-1, stdin); //lee los espacios y los intros
      //scanf(" %s", buffer); no lee los espacios
      //printf("%i", strlen(buffer)); nos dice la longitud de buffer
      len = strlen (buffer);
      lista[p] = (char *) malloc (len);
      strncpy (lista[p], buffer, len);
      lista[p][len-1] = '\0';
  }

  for (int p=0; p<2; p++)
      printf("%s\n", lista[p]);
  for (int p=0; p<2; p++)
      free(lista[p]);

  free(lista);

  return EXIT_SUCCESS;
}
