#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#define N 0x100
#define FN "benavente.txt"
#define NUMLETRAS ('z' - 'a' + 1)

int  main(int argc, char *argv[]){
    long inicio, fin;
    long d;
    int total = 0;
    char * texto;
    char letra;
    char frase[N];
    char contador;
    char resultado;
    FILE *pf;
    unsigned freq[NUMLETRAS];
    bzero(freq, sizeof(freq));

    if ( !(pf = fopen (FN, "rb")) ){
            fprintf (stderr, "Ay!\n");
            return EXIT_FAILURE;
        }

    fgets(frase, N, stdin);

    contador = strlen(frase);

    inicio = ftell(pf);
    fseek(pf, 0, SEEK_END);
    fin = ftell(pf);
    rewind (pf);
    d = fin - inicio;
    texto = (char *) malloc (d+1);
    fread (texto, sizeof(char), d, pf);
    *(texto + d ) = '\0';

    for (const char *dedo = texto; *dedo != '\0'; dedo++, total++){
            letra = tolower(*dedo);
            if (letra >= 'a' && letra <= 'z')
                freq[letra - 'a']++;
        }

    for (int c='a'; c<='z'; c++)
        printf ("%c: %.2lf%%\n", c, (double) 100. * freq[c-'a'] / total);

    for(const char *i = texto; *i != '\0'; i++){
          if(i == frase)
              resultado++;
        }

    if(resultado == contador){
            printf("La palabra %s se encuentra en el texto\n", frase);
        }else{
                printf("La palabra %s no se encuentra en el texto\n", frase);
        }

    free (texto);
    fclose (pf);
    return EXIT_SUCCESS;
}
