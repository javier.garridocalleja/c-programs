#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define FILENAME "NAME OF THE FILE"

int main(int argc, char *argv[]){

    FILE *pf;
    char *text;
    int c;

    if( argc < 2 ) {
            if ( !(pf = fopen (FILENAME, "rb")) ) {
                        fprintf(stderr, "I didn't find your file\n");
                        return EXIT_FAILURE;
                    }
        }
    else {
            if ( !(pf = fopen (argv[1], "rb")) ) {
                        fprintf(stderr, "I didn't find your file\n");
                        return EXIT_FAILURE;
                    }
        }

    printf("Put the word to search: ");
    scanf(" %ms", &text);
    printf("\n");
    do {
            c = getc(pf);
            if( c == text[0] || tolower(c) == text[0] ) {
                 if( strlen(text) == 1 )
                      printf("There is a  %c in %li\n", c, ftell(pf));
                 else
                   for(int i=1; i<strlen(text); i++) {
                        if( getc(pf) != text[i] ) break;
                             if( i == strlen(text ) - 1)
                          printf("The word %s is in %li\n", text, ftell(pf) - strlen(text)+1);
                     }
            }
       }while (c != EOF);

        if( c != text[0] || tolower(c) != text[0] )
            printf("No se ha encontrado la palabra %s\n", text);

      return EXIT_SUCCESS;
}

