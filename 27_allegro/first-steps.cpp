#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>

void init();
void deinit();

int main() {
  init();

  while(!KEY[KEY_ESC]) {

  }

  deinit();
  return 0;
}
EN_OF_MAIN()

void init() {
  int depth, res;
  allegro_init();
  depth = desktop_color_depth();
  if(depth == 0) depth = 32;
  set_color_depth(depth);

  res = set_gfx_mode(GFX_AUTODETECT_WINDOWED, 640, 480, 0, 0);
  if(res != 0) {
    allegro_message(allegro_error);
    exit(-1);
  }
  install_timer();
  install_keyboard();
  install_mouse();
}

void deinit(){
  clearenv();
}
