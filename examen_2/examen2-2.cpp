#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define MAX 10
#define INC 0.001

/* Función punto de entrada*/
int main(){
  double f=0;
  int potencia = 5;
  int resultado = 1;
  double x, ultimo_valor;

  double c[MAX] = {1,2,3,4,5};

  printf("Limite inferior: ");
  scanf(" %lf", &x);
  for (int t=x; ultimo_valor*f>=0; t+=INC){
      ultimo_valor = f;
  for(int termino=0; termino<MAX; termino++)
      f += c[termino] * pow(x, termino);
  }
  printf("f(%2.lf) = %.2lf\n", x, f);
  /*
  for(int vez=0; vez<potencia; vez++)
      resultado *= x;
  */

  return EXIT_SUCCESS;
}
