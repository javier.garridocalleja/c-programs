#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada*/
int main(){
  int num;
  printf("Introduzca un numero: ");
  scanf("%d", &num);
  /*int num2=45;*/
  printf("%d =", num);
  for (int i=2; num>1; i++){
    while (num % i ==0){
      printf("%d * ", i);
      num /=i;
    }
  }
  /*
  for (int i=2; num2>1; i++){
      while (num2 % i ==0){
      printf("%d * ", i);
      num2 /=i;
      }
  }
  */
  return EXIT_SUCCESS;
}
