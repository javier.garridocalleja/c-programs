#include <stdlib.h>
#include <stdio.h>

int main(){

  int numerador;
  int denominador;

  printf("Introduce un numerador: ");
  scanf("%i", &numerador);
  printf("Introduce un denominador: ");
  scanf("%i", &denominador);
  printf("\n");
  for (int pd=numerador/2; pd>1; pd--)
    if (numerador % pd == 0 && denominador % pd ==0){
          numerador /= pd;
          denominador /= pd;
        }
  printf ("La fraccion irreducible es: %i / %i \n", numerador, denominador);

  return EXIT_SUCCESS;
}
