#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>

#define MAX 0x100
#define LMUERTE 10

bool es_mala(char letra){
  return true;
}

int main(int argc, char *argv[]){
  char letra;
  char *malas, *letras;
  int total_malas=0, total_letras=0;

  malas = (char *) malloc (LMUERTE + 1);
  letras = (char *) malloc (MAX + 1);
  bzero (malas, LMUERTE + 1);
  bzero (letras, MAX + 1);

  while (total_malas < LMUERTE){
      system("clear");
      system("toilet -f pagga AGOGADO");
      printf("\n");
      printf("Letras: " "\x1B[32m" "%s \x1B[0m \n", letras);
      printf("Malas: ""\x1B[31m" "%s \x1B[0m \n", malas);
      printf("===================\n\n");

      printf("\x1B[36m Letra: ");
      letra = getchar();
      __fpurge (stdin);
      printf("\x1B[0m" "\n");
      if(strchr (letras,letra))
          continue;

      if (es_mala(letra))
          *(malas + total_malas++) = letra;
      *(letras + total_letras++) = letra;

  }

  free(letras);
  free(malas);
  return EXIT_SUCCESS;
}
