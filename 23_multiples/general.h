#ifndef __GENERAL_H__
#define __GENERAL_H__

#define MAX 0x04

enum { no, lleno, vacio, ERRORES };

struct TCola {
    int data[MAX];
    int base;
    int cima;
};

#endif
