#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 0x100
#define FILENAME "diccionario.txt" 

/* Función punto de entrada*/

int main(int argc, char *argv[]){
  FILE *pf;
  char buffer[N];
  int len, i;
  char **palabra = NULL;

  if ( !(pf=fopen (FILENAME, "w")) ){
      fprintf(stderr, "Can't open this file");
      return EXIT_FAILURE;
  }

  for(i=0;strcmp(buffer, "final\n"); i++){
      palabra = (char **) realloc (palabra, (i+1) * sizeof(char*));
      printf("Palabra: ");
      fgets (buffer, N-1, stdin);
  
      len = strlen(buffer);
      palabra[i] = (char *) realloc(palabra[i], len);
      strncpy(palabra[i], buffer, len);
      palabra[i][len-1] = '\0';
  }

  free(palabra[i-1]);
  palabra[i-1] = NULL;

  // imprime todas las palabras que se han introducido
  for(int i=0; palabra[i]; i++){
      printf("%s\n", palabra[i]);
  }

  // libera en memoria todas las palabras que se han introducido
  for(int i=0; palabra[i]; i++){
      free(palabra[i]);
  }

  free (palabra);
  fclose(pf);

  return EXIT_SUCCESS;
}
