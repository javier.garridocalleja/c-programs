#include <stdio.h>
#include <stdlib.h>

enum {
  triangulo,
  cuadrado,
  rectangulo,
  rombo,
};
/* Función punto de entrada*/
int main(){
  int menu, base, altura, lado, d_mayor, d_menor, resultado;

  system ("toilet -fpagga AREA");
  
  printf("1. Triangulo\n");
  printf("2. Cuadrado\n");
  printf("3. Rectangulo\n");
  printf("4. Rombo\n");

  printf("Elige una opcion: ");
  scanf(" %i", &menu);

  switch(menu){
      case 1:
      printf("Has elegido el triangulo\n");
      printf("Introduce la base: \n");
      scanf(" %i", &base);
      printf("Introduce la altura: \n");
      scanf(" %i", &altura);
      printf("Área triangulo = (base x altura)/2 \n");
      resultado = (base * altura)/2;
      printf("El resultado es %i\n", resultado);
      break;

      case 2:
      printf("Has elegido el cuadrado\n");
      printf("Introduce el lado: \n");
      scanf(" %i", &lado);
      printf("Área del cuadrado = lado x lado\n");
      resultado = lado * lado;
      printf("El resultado es %i\n", resultado);
      break;

      case 3:
      printf("Has elegido el rectángulo\n");
      printf("Introduce la base: \n");
      scanf(" %i", &base);
      printf("Introduce la altura: \n");
      scanf(" %i", &altura);
      printf("Área del rectangulo = base x altura\n");
      resultado = base * altura;
      printf("El resultado es %i\n", resultado);
      break;

      case 4:
      printf("Has elegido el rombo\n");
      printf("Introduce la diagonal mayor: \n");
      scanf(" %i", &d_mayor);
      printf("Introduce la diagonal menor: \n");
      scanf(" %i", &d_menor);
      printf("Área del rombo = (Diagonal mayor x diagonal menor) / 2 \n");
      resultado = (d_mayor * d_menor)/2;
      printf("El resultado es %i\n", resultado);
      break;
  }

	return EXIT_SUCCESS;
}
