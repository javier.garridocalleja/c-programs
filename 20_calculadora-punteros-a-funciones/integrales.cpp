#include <stdio.h>
#include <stdlib.h>

#define INC 0.0005

double parabola (double x){ return x * x; }
double integral (double li, double ls, double (*pf)(double) ){
  double area = 0;
  for(double x=li; x<ls; x+=INC){
      area += INC * (*pf)(x);
  }
}


int main(int argc, char *argv[]){

  double lims, limi;
  /* puntero a la funcion parabola */
  double (*pp)(double){ &parabola};
  /* puntero a la función integral */
  double (*pi)(double, double, double(double)){&integral};
  /* se pide el limite inferior y el superior */
  printf("Introduce el limite superior: ");
  scanf(" %lf", &lims);
  printf("Introduce el limite inferior: ");
  scanf(" %lf", &limi);
  /* se imprime el area */
  printf("El area es: %lf\n", (*pi) (lims, limi, &parabola));

  return EXIT_SUCCESS;
}
