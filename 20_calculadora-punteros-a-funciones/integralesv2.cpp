#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INC 0.0005

double pol (double *coef, int grado, double x) {
    int lon =  (grado+1) * sizeof (double);
    double *copia = (double *)malloc ( lon );
    double resultado = 0;
    memcpy (copia, pol, lon);

    for (int i=0; i<grado; i++)
        for (int celda=0; celda<=i; celda++)
            *(copia + celda) *= x;

    for(int i=0; i<=grado;i++)
        resultado += *(copia + i);

    free(copia);
}
// pol evalua el valor de un polinomio de coeficiente coef en el punto x

double integral (double li, double ls, double (*pf)(double) ){
  double area = 0;
  for(double x=li; x<ls; x+=INC)
      area += INC * (*pf)(x);
  return area;
}

double parabola (double x){
    double coef[]={1,0,0};
    return pol(coef, 2, x);
}
int main(int argc, char *argv[]){
    printf("f(2) = %lf", parabola(1));
    printf("f(3) = %lf", parabola(3));
    printf("El area es: %lf\n", integral (1, 3, &parabola));

    return EXIT_SUCCESS;
}
