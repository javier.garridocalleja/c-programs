#include <stdio.h>
#include <stdlib.h>

double factorial (int n){
  if(n==0)
      return 1;
  return n * factorial (n-1);
}

/* Función punto de entrada*/

int main(int argc, char *argv[]){
  int n=0;
  double E=0;

  for(n=0; n<10; n++){
    E = E + (1.0 / factorial(n));
  printf("%.5f\n", E);
  }

  return EXIT_SUCCESS;
}
