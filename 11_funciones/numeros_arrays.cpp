#include <stdio.h>
#include <stdlib.h>

#define N 10

int cuadrado (int n){
  return n * n;
}
void rellena (int l[N]){
  for(int n=0; n<N; n++)
      l[n] = cuadrado (n);
}

/* Función punto de entrada*/
int main(){
  int q[N];
  rellena(q);

  for(int i=0; i<N; i++)
      printf("%d\n", q[i]);

  return EXIT_SUCCESS;
}
