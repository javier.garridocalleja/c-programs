#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada*/
int main(){
  int numero;
  printf("Introduce un numero: \n");
  scanf(" %x", &numero);
  printf("Has introducido el numero %x\n", numero);
  if (numero%3==0){
    printf("El numero %x es multiplo de 3\n", numero);
  }
	return EXIT_SUCCESS;
}
