#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

char *program_name;
void print_usage(int exit_code){
  FILE *f=stdout;
  if(exit_code !=0)
      f = stderr;
  fprintf(f, "Ayuda po favoh\n", program_name);
  exit(exit_code);
}

int entrada_numero(int numero, int argc, char *argv[]){


    if(argc < 2)
        print_usage(1);
    numero = atoi(argv[1]);

    return numero;
}


int entrada_profundidad(double profundidad, int argc, char *argv[]){

    if(argc < 2)
        print_usage(1);
    numero = atoi(argv[1]);

    return profundidad;
}

double factorial (double numero){
  if(profundidad == 0)
      return entrada_numero;
  return numero + 1./factorial(numero, profundidad - 1);
}

/* Función punto de entrada*/
int main(int argc, char *argv[]){
  int numero;
  double profundidad;
  program_name = argv[0];

  numero = entrada_numero(numero, argc, argv);
  profundidad = factor(numero);

  printf("El factorial es %i ", numero);
  return EXIT_SUCCESS;
}
