#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
char *program_name;
void print_usage(int exit_code){
  FILE *f=stdout;
  if(exit_code !=0)
      f = stderr;
  fprintf(f, "Ayuda po favoh\n", program_name);
  exit(exit_code);
}

int entrada(int numero, int argc, char *argv[]){


    if(argc < 2)
        print_usage(1);
    numero = atoi(argv[1]);

    return numero;
}

int factor(int numero){
    if(numero==0){
      return -1;
    }
  return numero * factor(numero-1);
}

/* Función punto de entrada*/
int main(int argc, char *argv[]){
  int numero;
  program_name = argv[0];

  numero = entrada(numero, argc, argv);
  numero = factor(numero);

  printf("El factorial es %i ", numero);
  return EXIT_SUCCESS;
}
