#include <stdio.h>
#include <stdlib.h>

#define N 0x100
#define NOMBRE "fichero.txt"

struct TEstrella {
  double ascension_recta;
  double declinacion;
  double distancia;
  double masa;
  int edad;
  int brillo;
};

struct TPila {
  double data[N];
  int cima;
};

void push(struct TEstrella *p, nuevo) {
  p->data[p->cima]=nuevo;
  p->cima++;
}

void preguntar_veces(int veces) {
  struct TEstrella f;

  printf("¿Cuantas estrellas desea introducir?\n");
  scanf("%i", &veces);

  for(int i=1; i<=veces; i++) {
    printf("Introduce la ascension recta: ");
    scanf("%lf", &f.ascension_recta);
    printf("Introduce la declinacion: ");
    scanf("%lf", &f.declinacion);
    printf("Introduce la distancia: ");
    scanf("%lf", &f.distancia);
    printf("Introduce la masa: ");
    scanf("%lf", &f.masa);
    printf("Introduce la edad: ");
    scanf("%i", &f.edad);
    printf("Introduce el brillo: ");
    scanf("%i", &f.brillo);

    printf("Has llegado al final de la estrella %i\n", i);
    printf("\n");

    push(&f, nuevo);
   struct TEstrella *p  = (struct TEstrella *)malloc(sizeof(struct TEstrella));
 //  fwrite(f, sizeof(struct TEstrella *), 1, p);
  }
}

int main(int argc, char *argv[]){
    FILE *p;
    int veces, nuevo;
    struct TEstrella *f;
    struct TPila pila;

    preguntar_veces(veces);


    if((p=fopen(NOMBRE, "w")) == NULL) {
        fprintf(stderr, "No se ha podido encontrar el archivo %s", NOMBRE);
        return EXIT_FAILURE;
    }
  
    free(f);
    fclose(p);



    return EXIT_SUCCESS;
}
