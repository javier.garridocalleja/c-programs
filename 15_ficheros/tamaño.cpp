#include <stdio.h>
#include <stdlib.h>

#define FILENAME "volcado.txt"

int main(int argc, char *argv[]){
  FILE *pf;
  int pos;
  long int inicio, fin, distancia;
  if ( !(pf = fopen (FILENAME, "r")) ){
    fprintf(stderr, "No se ha podido leer el fichero");
    return EXIT_FAILURE;
  }


  inicio = ftell(pf);
  fseek (pf, 0, SEEK_END);
  fin = ftell (pf);
  distancia = fin - inicio;
  printf(" Inicio: %li\n"
         " Fin: %li\n"
         " Distancia: %li\n",
         inicio, fin, distancia);


  fclose(pf);

  return EXIT_SUCCESS;
}
