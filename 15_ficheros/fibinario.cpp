#include <stdio.h>
#include <stdlib.h>

#define N 9
#define DUMP "fibonacci.dat"


int rellenar (int fibo[N], int i){
  if (i == 1){
    fibo[0] = 1;
    return fibo[1] = 1;
  }
  return fibo[i] = rellenar (fibo, i-1) + fibo[i-2];
}

int main(int argc, char *argv[]){
  FILE *pf;
  int fibo[N];
  int n = sizeof(fibo) / sizeof (int);

  rellenar (fibo, N-1);
  if ( !(pf = fopen (DUMP, "wb")) ){
    fprintf(stderr, "El fichero no se ha podido abrir");
    return EXIT_FAILURE;
  }

  fwrite (fibo, sizeof (int), n, pf);

  fclose(pf);

  return EXIT_SUCCESS;
}
