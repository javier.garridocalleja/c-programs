#include <stdio.h>
#include <stdlib.h>

#define NOMBRE "cancion.txt"

const char * song= "\n\
    Don't think sorry is easily said\n\
    Don't try turning tables instead\n\
    You've taken lots of chances before\n\
    But ain't gonna give anymore\n\
    Don't ask me, that's how it goes\n\
    \n\
    'Cause part of me knows what you're thinkin'\n\
    Don't say words you're gonna regret\n\
    Don't let the fire rush to your head\n\
    I've heard the accusation before\n\
    And I ain't gonna take any more\n\
    Believe me, the sun in your eyes\n\
    Made some of the lies worth believing\n\
    I am the eye in the sky\n\
    \n\
    Looking at you\n\
    I can read your mind\n\
    I am the maker of rules\n\
    Dealing with fools\n\
    I can cheat you blind\n\
    And I don't need to see anymore\n\
    To know that\n\
    \n\
    I can read your mind (looking at you)\n\
    I can read your mind (looking at you)\n\
    I can read your mind (looking at you)\n\
    I can read your mind\n\
";

void print_usage (){
  printf("Esto se usa asi\n");
}

void informo (const char *mssg){
  print_usage ();
  fprintf(stderr, "%s\n", mssg);
  exit(1);
}

int main(int argc, char *argv[]){

  FILE *fichero; // puntero a un fichero

  if ( !(fichero = fopen ( NOMBRE , "w")) )
      informo ("No se ha podido abir el fichero.");  // se crea el tubo
  fprintf (fichero, "%s", song);
  fclose (fichero); //se cierra el tubo


  return EXIT_SUCCESS;
}
