#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define OUT "cambio_simbolos.txt"
#define N 0x100

char entrada(char buffer[N], int argc, char *argv[]){
  if(argc < 1)
      return EXIT_FAILURE;

  strcpy(buffer, argv[1]);
  return buffer[N];
}

int main(int argc, char *argv[]){
  FILE *res;
  char buffer[N];
  char *pf;

  /*ENTRADA*/ buffer[N] = entrada(buffer, argc, argv);
  pf = buffer;

  while(*pf != '\0'){
    if(*pf == 'i') *pf = '!';
    pf++;
  }

  if(!(res = fopen (OUT, "w")) )
      return EXIT_FAILURE;
  fprintf(res, "%s\n", buffer);
  fclose(res);

  return EXIT_SUCCESS;
}
