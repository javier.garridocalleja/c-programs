#include <stdio.h>
#include <stdlib.h>

#define N 10
#define MAX 0x100

int main(int argc, char *argv[]){
    char nombre;
    FILE *pf;
    const char * volcado;


    if ( !(pf = fopen(volcado, "r")) ){
        fprintf(stderr, "No he podido abrir el fichero. \n");
        return EXIT_FAILURE;
    }
    for(int i=0; i<N; i++)
      fgets( nombre[i], MAX, pf);

    for(int i=0; i<N; i++)
        printf("Fila %i: %s\n", i, nombre[i]);

    fclose(pf);


  return EXIT_SUCCESS;
}
