#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Función punto de entrada*/
int main(){
  /*declaracion de variables*/
  double angulo, a0, a1, resultado, resultado1, coseno, seno, r;

  /*ENTRADA*/
  printf("Introduce un angulo: ");
  scanf("%lf", &angulo);

  printf("Introduce las coordenadas del punto: ");
  scanf("%lf,%lf", &a0, &a1);
  /*OPERACIONES*/
  r = angulo * M_PI/180;
  coseno = cos(r);
  seno = sin(r);

  resultado = (a0 * coseno) + (a1 * - seno);
  resultado1 = (a0 * seno) + (a1 * coseno);

  /*SALIDA*/
  printf("%.3lf,%.3lf\n", resultado, resultado1);

  return EXIT_SUCCESS;
}
