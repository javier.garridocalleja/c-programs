#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada*/
int main(){
  int producto, numero[4];

  printf("Introduce un vector: ");
  scanf(" %d,%d", &numero[1], &numero[2]);
  printf("Introduce otro vector: ");
  scanf(" %d,%d", &numero[3], &numero[4]);

  producto = ((numero[1] * numero[4]) - (numero[2] * numero[3]));

  printf("El producto vectorial es: %d\n", producto);


  return EXIT_SUCCESS;
}
