#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada*/
int main(){
  double r, r1, r2, a00, a10, a01, a11, a02, a12, b00, b10, b20;
  double a[3][3];

  printf("Introduce el valor de a00: ");
  scanf("%lf", &a00);

  printf("Introduce el valor de a10: ");
  scanf("%lf", &a10);

  printf("Introduce el valor de a01: ");
  scanf("%lf", &a01);

  printf("Introduce el valor de a11: ");
  scanf("%lf", &a11);

  printf("Introduce el valor de a02: ");
  scanf("%lf", &a02);

  printf("Introduce el valor de a12: ");
  scanf("%lf", &a12);

  printf("\n");
  printf("Matriz\n");
  printf("| %.lf    %.lf    %.lf |\n", a00, a01, a02);
  printf("| %.lf    %.lf    %.lf |\n", a10, a11, a12);

  printf("Introduce el valor del vector b00: ");
  scanf("%lf", &b00);

  printf("Introduce el valor del vector b10: ");
  scanf("%lf", &b10);

  printf("Introduce el valor del vector b11: ");
  scanf("%lf", &b20);

  printf("\n");
  printf("Matriz\n");
  printf("| %.lf |\n", b00);
  printf("| %.lf |\n", b10);
  printf("| %.lf |\n", b20);
  printf("\n");

  printf("Operacion\n");

  printf("| %.lf    %.lf    %.lf |   | %.lf |\n", a00, a01, a02, b00);
  printf("| %.lf    %.lf    %.lf |   | %.lf |\n", a10, a11, a12, b10);
  printf("                       | %.lf |\n", b20);



  r1 = ((a00 * b00) + (a01 * b10) + (a02 * b20));
  r2 = ((a10 * b00) + (a11 * b10) + (a12 * b20));

  printf("La matriz es: %.lf entre %.lf \n", r1, r2);

  return EXIT_SUCCESS;
}
