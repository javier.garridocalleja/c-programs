#include<stdio.h>
#include<stdlib.h>

int main (){
  int c,f,l;
  printf("Introduce el lado: ");
  scanf("%i", &l);
  for(int f=0; f<l;f++){
    for(int c=0; c<l; c++)
        if(c==0 || c==l-1 || f==0 || f==l-1 || c==f || f==c || f+c==l-1)
            printf("*");
        else
            printf(" ");
    printf("\n");
  }
  return EXIT_SUCCESS;
}
