#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* Función punto de entrada*/
int main(){
	fprintf(stderr, "\a"); //stderr no esta buferizado, es el tubo de errores. FPRINTF imprime caracteres en el tubo que nosotros digamos.
	usleep(100000);
	fputc('\a', stderr);
	usleep(100000);
	printf("\a\n");

	return EXIT_SUCCESS;
}
