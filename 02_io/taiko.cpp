#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define retardonegra 150000
#define retardocorchea 200000
#define retardoblanca 250000
#define retardofusa 350000
#define retardosemicorchea 500000
#define retardoredonda 7500000

int main(){

	usleep(retardoblanca);
	printf("\a\n");

	usleep(retardocorchea);
	printf("\a\n");
	
	usleep(retardonegra);
	printf("\a\n");

	usleep(retardonegra);
	printf("\a\n");

	usleep(retardonegra);
	printf("\a\n");

	usleep(retardosemicorchea);
	printf("\a\n");

	usleep(retardocorchea);
	printf("\a\n");
	
	return EXIT_SUCCESS;
}
