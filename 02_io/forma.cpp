#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada*/
int main(){

	printf("%c", 97); //codigo ascii
	printf("%c", 0x61); //codigo hexadecimal
	printf("%c", 'a'); //constante de tipo caracter
	
	puts(""); //salto de linea
	printf("%i", 97); //%i actua como numero
	printf("\n");
	//scanf(\"%i\");
	printf("%lf\n", 3.2); //imprime numeros reales con comas

	int numero;
	printf("Num: ");
	scanf(" %i", &numero);
	printf("Numero => [%p]: %i\n", &numero, numero);
	printf("Linea %i\n", 47);
	
	int dia, annio;
	printf("Nacimiento dd/mm/aaaa: ");
	scanf(" %i/%*i/%i", &dia, &annio); //Caracter de supresion de asignacion
	
	char hex[32];
	scanf(" %[0-9a-fA-F]", hex);


	return EXIT_SUCCESS;
}
