#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada*/
int main(){
	int numero, i;
	printf("Dime un numero: ");
	scanf("%i", &numero);

	for(i=0; i<=10; i++){
		printf( "\n %i * %i = %i\n", numero, i, i * numero);
	}
	return EXIT_SUCCESS;
}
