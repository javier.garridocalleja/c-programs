Secuencia de Escape con nombre: 
\t: tabulador horizontal 
\v: tabulacion vertical
\a: campana alerta
\r: retorno de carro
\b: retroceso
\f: avance de pagina
\n: salto de linea

Powerless
\\: barra invertida (Muestra el caracter barra invertida)
\?: interrogación (Muestra el caracter interrogacion)
\": comilla doble (Muestra el caracter comilla doble)
\': comilla simple (Muestra el caracter comilla simple)

Pongo el numero
\xnn HEXADECIMAL
\0nn OCTAL
