#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define VECES 12
#define BTU 10000 /*Basic Time Unit*/

/* Función punto de entrada*/
int main(){
	//bucle for: debe valer para repetir cosas
	int duracion[VECES] = {0, 2, 8, 2, 1, 8, 2, 1, 2, 1, 2, 2 };
	for (int i=0; i<VECES; i++){
		usleep(duracion[i] * BTU);
		fputc('\a', stderr);

	}

	return EXIT_SUCCESS;
}
