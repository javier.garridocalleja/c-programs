#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada*/
int main(){
	int numero;
	printf("Ingrese a un numero: ");
	scanf(" %i", &numero);

	printf(" El numero en hexadecimal es: 0x%X\n", numero);
	printf("\n");
	return EXIT_SUCCESS;
}
