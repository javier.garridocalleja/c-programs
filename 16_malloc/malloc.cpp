#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada*/

int main(int argc, char *argv[]){

  int a;
  char *p;

  printf("Ingrese el numero de caracteres de la cadena: ");
  scanf("%d", &a);
  getchar();
  p = (char *) malloc(a*sizeof(char));

  printf("Se ha reservado %d caracteres", a);
  printf("\n");

  printf("Introduzca la cadena: ");
  scanf("%s", p);

  if(sizeof(p)>a){
    printf("Debe introducir la cadena de caracteres con la longitud introducida");
  }else{
  printf("El valor de la cadena almacenada es: %s\n ", p);
  }




  return EXIT_SUCCESS;
}
