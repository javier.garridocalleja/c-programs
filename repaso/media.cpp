#include <stdio.h>
#include <stdlib.h>
#define N 1000
/* Función punto de entrada*/
int main(){
    double nota[N], media, entrada;
    int n_alumnos = 0;
    do{
    printf("Nota: ");
    scanf(" %lf", &entrada);
    if (entrada >=0)
        nota[n_alumnos++] = entrada;
    } while (entrada >=0);

  for(int i=0; i<n_alumnos; i++){
      media += nota[i];
  }
  media /= n_alumnos;

  printf("Media: %.2lf\n", media);

  return EXIT_SUCCESS;
}
