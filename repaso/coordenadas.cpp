#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Función punto de entrada*/
int main(){
  int vector1, vector2;
  double arcotangente;

  printf("Introduce un vector: ");
  scanf("%i,%i", &vector1, &vector2);

  arcotangente = atan2 (vector1, vector2);

  printf("El angulo mide: %.2lf grados\n ", arcotangente);

  return EXIT_SUCCESS;
}
