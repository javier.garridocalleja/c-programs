#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Función punto de entrada*/
int main(){
  int vector1, vector2, vector3;
  double modulo;

  printf("Dime un vector: ");
  scanf("%i", &vector1);
  printf("Dime otro vector: ");
  scanf("%i", &vector2);
  printf("Dime otro vector: ");
  scanf("%i", &vector3);

  modulo = sqrt( (pow(vector1,2) + pow(vector2,2) + pow(vector3,2)) );
  printf("El modulo es %.2lf\n", modulo);
  return EXIT_SUCCESS;
}
