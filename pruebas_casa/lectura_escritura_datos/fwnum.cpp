#include <stdio.h>
#include <stdlib.h>

#define N 100

int main(int argc, char *argv[]){
  FILE *p;
  int num[N];
  int numlee, numesc;
  int n = 50;

  for(int i=0; i<=N-1; i++)
      num[i]=n++;
  if ( (p = fopen("enteros", "wb")) !=NULL){
    numesc = fwrite( (char *) num, sizeof(int), N, p);
    printf("\nNumero de items escritos: %d", numesc);
  }else{
    fprintf(stderr, "No puede abrirse el archivo \"enteros\"");
    return EXIT_FAILURE;
  }
  fclose(p);

  p=fopen("enteros", "rb");
  numlee = fread( (char *)num, sizeof(int), N, p);
  printf("\nNumero de items leidos %d", numlee);
  printf("\nEl primer item leido es %d\n", num[0]);
  fclose(p);

  return EXIT_SUCCESS;
}
