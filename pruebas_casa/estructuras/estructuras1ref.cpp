// PASO DE ESTRUCTURAS POR REFERENCIA
#include <stdio.h>
#include <stdlib.h>
#define APE 40
#define NOM 20

void nomina(long *, int *, long  *);
long nomina (long *sueldo, int *extras, long *pre_extra, long *res){
  *res = *sueldo + ((*extras) * (*pre_extra));
  *res -= (0.15 * (*res));
}
int main(int argc, char *argv[]){
  long resultado;
  struct{
    char apellido[APE];
    char nombre[NOM];
    long sueldo;
    int h_extras;
    long precio_he;
  }
  per;

  printf("APELLIDOS: ");
  scanf("%s",per.apellido);
  printf("NOMBRE: ");
  scanf("%s",per.nombre);
  printf("SUELDO: ");
  scanf("%ld", &per.sueldo);
  printf("HORAS EXTRA: ");
  scanf("%d", &per.h_extras);
  printf("PRECIO HORA EXTRA: ");
  scanf("%ld", &per.precio_he);

  nomina(&per.sueldo, &per.h_extras, &per.precio_he, &resultado);
  printf("\nEmpleado: %s, %s\n Mensualidad %ld\n",
          per.apellido, per.nombre, resultado);
  return EXIT_SUCCESS;
}
