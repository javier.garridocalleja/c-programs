#include <stdio.h>
#include <stdlib.h>

#define APE 35
#define NOM 25
#define CUR 4

void crear();
struct alumnos{
  char apellido[APE];
  char nombre[NOM];
  int edad, num;
  char curso[CUR];
};

void crear(struct alumnos *f, int num, char nom[30]){
  int i, j;
  FILE *fi;
  char *ap[35], *no[25];

  ap[0]= (*f).apellido;
  no[0]= (*f).nombre;

  if( (fi = fopen(nom, "w")) == NULL){
    printf("No se ha podido abrir el archivo %s", nom);
    return ;
  }
  for(i=0; i<num;i++){
    fflush(stdin);
    printf("APELLIDOS: ");
    scanf("%s", (*f).apellido);
    printf("NOMBRE: ");
    scanf("%s", (*f).nombre);
    printf("EDAD: ");
    scanf("%d", &(*f).edad);
    printf("NUMERO: ");
    scanf("%d", &(*f).num);

    fwrite(f, sizeof(struct alumnos), 1, fi);
    for(j=0; j<APE;j++)
        (*ap)[j] = '\40';
    for(j=0; j<NOM;j++)
        (*no)[j] = '\40';
  }
  fclose(fi);
}
int main(int argc, char *argv[]){
    int numero_alumno;
    char nom_fi[30];
    struct alumnos al;

    printf("NUMERO DE ALUMOS: ");
    scanf("%d",&numero_alumno);

    fflush(stdin);
    printf("Nombre del fichero de alumnos: ");
    scanf("%s", nom_fi);

    crear(&al, numero_alumno, nom_fi);

    return EXIT_SUCCESS;
}
