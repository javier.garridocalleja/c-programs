// PASO DE ESTRUCTURAS POR VALOR
#include <stdio.h>
#include <stdlib.h>
#define APE 40
#define NOM 20

long nomina(long, int, long);
long nomina (long sueldo, int extras, long pre_extra){
  long total;
  total = sueldo + (extras * pre_extra);
  total -= (0.15 * total);
  return total;
}
int main(int argc, char *argv[]){
  long pago;
  struct{
    char apellido[APE];
    char nombre[NOM];
    long sueldo;
    int h_extras;
    long precio_he;
  }
  per;

  printf("APELLIDOS: ");
  scanf("%s",per.apellido);
  printf("NOMBRE: ");
  scanf("%s",per.nombre);
  printf("SUELDO: ");
  scanf("%ld", &per.sueldo);
  printf("HORAS EXTRA: ");
  scanf("%d", &per.h_extras);
  printf("PRECIO HORA EXTRA: ");
  scanf("%ld", &per.precio_he);

  pago= nomina(per.sueldo, per.h_extras, per.precio_he);
  printf("\nEmpleado:%s, %s\n Mensualidad: %ld\n",
          per.apellido, per.nombre, pago);

  return EXIT_SUCCESS;
}
