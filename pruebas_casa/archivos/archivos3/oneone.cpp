#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]){
  FILE *p;
  char NombreFichero[30];
  char caracter;

  printf("Introduzca el nombre del archivo de texto\n");
  scanf("%s", NombreFichero);

  if ( (p = fopen(NombreFichero, "r")) == NULL){
    fprintf(stderr, "Imposible abrir el archivo de texto %s\n", NombreFichero);
    return EXIT_FAILURE;
  }
  while(!feof(p))
      putc(getc(p), stdout);
  printf("\nSe ha encontrado el final del archivo\n");

  fclose(p);
  return EXIT_SUCCESS;
}
