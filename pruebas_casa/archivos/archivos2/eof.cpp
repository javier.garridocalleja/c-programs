#include <stdio.h>
#include <stdlib.h>

#define ARCHIVO "pepe"
int main(int argc, char *argv[]){
  FILE *f;
  int ch;

  if( (f = fopen(ARCHIVO, "rb")) == NULL){
    fprintf(stderr, "No se ha podido abrir el fichero");
    return EXIT_FAILURE;
  }
  
  do {
    ch = getc(f);
    printf("%d\n", ch);
  }while(ch!=EOF);

  printf("\n");
  if(ch == EOF)
      printf("EOF vale %d\n", ch);
  fclose(f);

  return EXIT_SUCCESS;
}
