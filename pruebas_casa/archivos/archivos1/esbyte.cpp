#include <stdio.h>
#include <stdlib.h>

#define ARCHIVO "pepe.txt"

int main(int argc, char *argv[]){
  FILE *Corriente;
  char Caracter;
  Caracter = 'a';
      if( (Corriente = fopen(ARCHIVO, "w")) == NULL){
        fprintf(stderr, "Imposible abrir el archivo");
        return EXIT_FAILURE;
      }
  fputc(Caracter, Corriente);
  fputc('z', Corriente);

  fclose(Corriente);

  if( (Corriente = fopen(ARCHIVO, "r")) == NULL){
    fprintf(stderr, "Imposible abrir el archivo");
    return EXIT_FAILURE;
  }
  Caracter = fgetc(Corriente);
  printf("%d", Caracter);
  Caracter = fgetc(Corriente);
  printf("%d", Caracter);
  Caracter = fgetc(Corriente);
  printf("%d", Caracter);
  fclose (Corriente);

  return EXIT_SUCCESS;
}
