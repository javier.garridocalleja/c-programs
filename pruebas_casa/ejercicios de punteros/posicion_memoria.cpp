#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada*/
int main(){
        int n = 10, *p_n = &n;
        float n2 = 10.5, *p_n2 = &n2;
        char n3 = 'a', *p_n3 = &n3;

        printf("Variable entera: \n");
        printf("Dato: %i", *p_n);
        printf("\nDireccion de memoria: %p", p_n);

        printf("\n\nVariable de tipo float: \n");
        printf("Dato: %.2f", *p_n2);
        printf("\nDireccion de memoria: %p", p_n2);

        printf("\n\nVariable de tipo caracter: \n");
        printf("Dato: %c", *p_n3);
        printf("\nDireccion de memoria: %p\n", p_n3);


	return EXIT_SUCCESS;
}
