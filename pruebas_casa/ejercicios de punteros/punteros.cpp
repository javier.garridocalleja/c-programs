#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada*/
int main(){
        int numero, *dir_numero;
        printf("Introduzca un numero: ");
        scanf(" %i", &numero);

        dir_numero = &numero; //Guarda la posicion de memoria 

        if(*dir_numero%2==0){
          printf("El numero %i es par.\n", *dir_numero);
          printf("La posicion de memoria donde se ha guardado el numero es: 0x%X\n", dir_numero);
        }
        else{
          printf("El numero %i es impar.\n", *dir_numero);
          printf("La posición de memoria donde se ha guardado el numero es: 0x%X\n", dir_numero);
        }
	return EXIT_SUCCESS;
}
