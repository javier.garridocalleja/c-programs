#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada*/
int main(){
        int medida, menu;

        printf("Introduzca la cantidad de kilogramos: ");
        scanf(" %i", &medida);

        printf("\n\t Unidades de medida de masa\n");
        printf("1. Hectogramos\n");
        printf("2. Decagramos\n");
        printf("3. Gramos\n");
        printf("4. Decigramos\n");
        printf("5. Centigramos\n");
        printf("6. Miligramos\n");
        printf("Indique la opción a la que desea pasar la unidad: ");
        scanf(" %i", &menu);

        switch(menu){
            case 1:
                medida = medida * 10;
                break;
            case 2:
                medida *= 100;
                break;
            case 3:
                medida *= 1000;
                break;
            case 4:
                medida *= 10000;
                break;
            case 5:
                medida *= 100000;
                break;
            case 6:
                medida *= 1000000;
                break;
        }
        printf("La nueva medida es: %i\n", medida);
	return EXIT_SUCCESS;
}
