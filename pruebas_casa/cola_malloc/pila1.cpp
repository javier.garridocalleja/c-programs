#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 3
#define TRUE 1

char *p_comerciante[MAX];
int numero=0;
int orden=0;

void introduce_comerciante();
void proximo_comerciante();
/*
 La funcion introduce_comerciante toma la memoria necesaria dentro del array comerciiante; si malloc se ejecuta correctamente, coloca al comerciante
 en la posición adecuada de la memoria
*/
void introduce_comerciante(){
  char comerciante[10], *puntero_bloque_memoria;
  while (numero < MAX) {
    printf("\n Codigo del comerciante %i=", numero+1);
    gets(comerciante);

      if(*comerciante==0) break;
    puntero_bloque_memoria=malloc (strlen(comerciante));
      if(puntero_bloque_memoria == NULL){
        printf("\n Espacio de memoria insuficiente");
        return;
      }
      strcpy(puntero_bloque_memoria, comerciante);
      if(*comerciante){
        if(numero==MAX){
          printf("\nSe ha llegado al maximo %i", MAX);
          return;
        }
        p_comerciante[numero]= puntero_bloque_memoria;
        numero++;
      }
  }
}
/*
 La funcion proximo_comerciante comprueba si puede sacar mas comerciantes de la cola, imprime el codigo del comerciate y prepara el indice del array para el
 siguiente comerciante
*/
void proximo_comerciante(){
  if(orden==numero){
    printf("\n No hay mas clientes");
    numero=0;
    orden=0;
    return;
  }
  if(p_comerciante[numero-1]==NULL)
      return;
  printf("\n Siguiente comerciante %s", p_comerciante[orden]);
  orden++;
}

int main(int argc, char *argv[]){
  while(TRUE){
    printf("\n MENU DE PROGRAMA DE COMERCIO");
    printf("\n --------------------------\n\n");
    printf("\tP  - ORDEN PEDIDOS.");
    printf("\tS  - SALIDA COMERCIANTE.");
    printf("\tX  - SALIR DEL PROGRAMA");
      switch(toupper(getche())){
      case 'P':
          introduce_comerciante();
          break;
      case 'S':
          proximo_comerciante();
          break;
      case 'X':
          exit(0);
    }
  }

  return EXIT_SUCCESS;
}
