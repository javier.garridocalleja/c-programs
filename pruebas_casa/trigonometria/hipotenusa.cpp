#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Función punto de entrada*/
int main(){
  float hipotenusa, cateto1, cateto2;

  printf("Introduzca dos catetos: ");
  scanf("%f,%f", &cateto1, &cateto2);

  hipotenusa = sqrt( pow(cateto1,2) + pow(cateto2,2) );

  printf("La hipotenusa del triangulo rectangulo es: %.2f\n", hipotenusa);


  return EXIT_SUCCESS;
}
