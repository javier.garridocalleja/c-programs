#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 0x100

/* Función punto de entrada*/

int main(int argc, char *argv[]){
  char nombre[N], *p_nombre;
  int longitud;

  printf("Introduce el nombre: ");
  scanf("%s", nombre);
  
  longitud = strlen(nombre);

  p_nombre = (char *) malloc(longitud + 1);

  strncpy(p_nombre, nombre, N);

  if(longitud < N)
      nombre[longitud] = '\0';
  else
      nombre[N-1] = '\0';

  printf("Nombre: %s\n", p_nombre);

  free (p_nombre);

  return EXIT_SUCCESS;
}
