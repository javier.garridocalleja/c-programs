#include <stdio.h>
#include <stdlib.h>


int main(int argc, char *argv[]){
  int numero;

  printf("Introduzca un numero: ");
  scanf("%i", &numero);

  (numero%2==0) ? printf("El numero es par") : printf("El numero es impar");
  printf("\n");

  return EXIT_SUCCESS;
}
