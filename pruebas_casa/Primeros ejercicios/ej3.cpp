#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada*/
int main(){
	int tabla,i;
	printf("Dime la tabla que quieres que imprima en la pantalla: ");
	scanf("%i", &tabla);
	printf("La tabla de %i es la siguiente:\n", tabla);
	for (int i=0; i<=10; i++){
		printf(" %i x %i = %i\n", tabla, i, i * tabla);
	}
	return EXIT_SUCCESS;
}
