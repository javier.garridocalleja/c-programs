#include <stdio.h>
#include <stdlib.h>

char * program_name;
void print_usage(int exit_code){
  FILE *f=stdout;
  if(exit_code !=0)
      f = stderr;
  fprintf(f, "Ayuda", program_name);
  exit(exit_code);
}

int entrada(int Num, int argc, char *argv[]){
  if(argc < 2)
      print_usage(1);
  Num = atoi(argv[1]);
  return Num;
}

int funcion_factorial(int Num){
  if(Num==0)
      return 1;
  else
      return (funcion_factorial(Num-1)*Num);
}

/* Función punto de entrada*/
int main(int argc, char *argv[]){
  int Num;
  program_name = argv[0];
  printf("Ingresa un numero para calcular el factorial: ");
  scanf("%d", &Num);
  printf("\n");

  printf("El factorial de %d es %d\n", Num, funcion_factorial(Num));
  return EXIT_SUCCESS;
}
