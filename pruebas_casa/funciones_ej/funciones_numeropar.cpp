#include <stdio.h>
#include <stdlib.h>

int comprobar(int numero){
  if(numero%2==0){
    return 0;
  }else{
      return 1;
  }
}

/* Función punto de entrada*/
int main(){
  int numero, x;

  printf("Digite un numero: ");
  scanf("%i", &numero);

  x = comprobar(numero);

  if(x == 0){
    printf("El numero es par\n");
  }else{
    printf("El numero es impar\n");
  }
  return EXIT_SUCCESS;
}
