#include <stdio.h>
#include <stdlib.h>

void comprobar (int numero){
  if(numero%2 == 0){
    printf("El numero es par\n");
  }else{
    printf("El numero es impar\n");
  }
}


/* Función punto de entrada*/
int main(){
  int numero, x;

  printf("Digite un numero: ");
  scanf("%i", &numero);

  comprobar(numero);

  return EXIT_SUCCESS;
}
