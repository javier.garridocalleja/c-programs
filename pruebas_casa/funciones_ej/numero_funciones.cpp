#include <stdio.h>
#include <stdlib.h>
void comprobar(int n);

void comprobar(int n){
  switch(n){
    	case 1:
            printf("Uno\n");
            break;
	case 2:
            printf("Dos\n");
            break;
	case 3:
            printf("Tres\n");
            break;
	case 4:
            printf("Cuatro\n");
            break;
	case 5:
            printf("Cinco\n");
            break;
	case 6:
            printf("Seis\n");
            break;
	case 7:
            printf("Siete\n");
            break;
	case 8:
            printf("Ocho\n");
            break;
	case 9:
            printf("Nueve\n");
            break;
	case 10:
            printf("Diez\n");
            break;
  }
}

int main(int argc, char *argv[]){

  int numero;
  printf("Introduce un numero: ");
  scanf("%i", &numero);
  while(numero<1 || numero>10){
    printf("Introduce un numero: ");
    scanf("%i", &numero);
  }
  comprobar(numero);

  return EXIT_SUCCESS;
}
