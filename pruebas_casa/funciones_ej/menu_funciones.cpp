#include <stdio.h>
#include <stdlib.h>
void sumar();
void restar();
void multiplicar();
void dividir();


void menu(){
  int opc;

  do{
  printf("1. Sumar\n");
  printf("2. Restar\n");
  printf("3. Multiplicar\n");
  printf("4. Dividir\n");
  printf("5. Salir\n");

  printf("Opcion: ");
  scanf("%i", &opc);

  switch(opc){
    case 1:
        sumar();
        break;
    case 2:
        restar();
        break;
    case 3:
        multiplicar();
        break;
    case 4:
        dividir();
        break;
  }
  }while(opc != 5);
}

void sumar(){
  int n1, n2, suma = 0;
  printf("Introduzca 2 numeros: ");
  scanf("%i %i", &n1, &n2);

  suma = n1 + n2;
  printf("Suma = %i\n", suma);
}
void restar(){
  int n1, n2, resta = 0;
  printf("Introduzca 2 numeros: ");
  scanf("%i %i", &n1, &n2);

  resta = n1 - n2;
  printf("Resta = %i\n", resta);
}

void multiplicar(){
  int n1, n2, multiplicacion = 0;
  printf("Introduzca 2 numeros: ");
  scanf("%i %i", &n1, &n2);

  multiplicacion = n1 + n2;
  printf("Multiplicacion = %i\n", multiplicacion);
}
void dividir(){
  int n1, n2, division = 0;
  printf("Introduzca 2 numeros: ");
  scanf("%i %i", &n1, &n2);

  division = n1 / n2;
  printf("Division = %i\n", division);
}


/* Función punto de entrada*/

int main(int argc, char *argv[]){
  menu();

  return EXIT_SUCCESS;
}
