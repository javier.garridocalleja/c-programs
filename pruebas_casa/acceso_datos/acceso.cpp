#include <stdio.h>
#include <stdlib.h>

#define N 0x100
#define cad "123456789ABCDEFGHIJK"


int main(int argc, char *argv[]){
  char cadena[N];
  FILE *p;
  int num;
  long desplaza;

  p = fopen("numeros", "wb+");
  fprintf(p, "%s", cad);
  desplaza = 7;
  
  fseek(p, desplaza, SEEK_SET);
  printf("Posicion: %li\n", ftell(p));
  fgets(cadena, 6, p);
  // La funcion fgets lee 6 caracteres, comenzando por el octavo (desplaza)
  // y los almacena en cadena, la funcion imprime 89ABCD
  printf("cadena = %s\n", cadena);
  printf("Al leer el puntero pasa a la siguiente posicion\n");

  fclose(p);
  return EXIT_SUCCESS;
}
