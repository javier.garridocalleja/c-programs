#include <stdio.h>
#include <stdlib.h>

//#define saldo_inicial 1000
/* Función punto de entrada*/
int main(){
        int saldo, reintegro, menu, anadir, saldo_inicial;
        printf("Bienvenido a su cajero virtual\n");
        printf("------------------------------\n");
        printf("Introduzca el saldo que quiere tener en euros(€): \n");
        scanf(" %i €", &saldo_inicial);
        printf("1. Ingreso en cuenta\n");
        printf("2. Reintegro\n");
        printf("3. Ver saldo de cuenta\n");
        printf("4. Salir\n");
        printf("Escoja una opcion: ");
        scanf(" %i", &menu);

        if(menu==1){
          printf("Ingrese la cantidad a depositar\n");
          scanf(" %i €", &anadir);
          saldo = saldo_inicial + anadir;
          printf("Cantidad disponible en cuenta %i €\n", saldo);
        }
        else if (menu==2){
          printf("Cuanto dinero desea retirar: \n");
          scanf(" %i €", &reintegro);
          if (reintegro>saldo_inicial){
            printf("La cantidad que has puesto es mayor al saldo de su cuenta: %i €\n", saldo_inicial);
            printf("Introduzca de nuevo la cantidad correspondiente\n", reintegro);
          }
          saldo = saldo_inicial - reintegro;
          printf("La cantidad disponible en la cuenta es: %i €\n", saldo);
        }
        else if (menu==3){
          printf("La cantidad disponible en cuenta es: %i €\n", saldo_inicial);
        }
        else if (menu==4){
          printf("Gracias por confiar en nosotros.\n");
          /*printf("¿Quiere volver a realizar otra operacion?(S/N): ");
          if(operacion==S){
              menu();
          }*/
        }
        else{
          printf("Ha introducido una opcion no valida\n");
        }
	return EXIT_SUCCESS;
}
