#include <stdio.h>
#include <stdlib.h>

#define saldo_inicial 1000
/* Función punto de entrada*/
int main(){
        int saldo, reintegro, menu, anadir, dinero_inicial;

        printf("Bienvenido a su cajero virtual\n");
        printf("------------------------------\n");
        printf("1. Ingreso en cuenta\n");
        printf("2. Reintegro\n");
        printf("3. Ver saldo de cuenta\n");
        printf("4. Salir\n");
        printf("Escoja una opcion: \n");
        scanf(" %i", &menu);
        printf("Indique cuanto dinero tiene en su cuenta bancaria: ");
        scanf(" %i", &dinero_inicial);
        switch(menu){
          case 1:
             printf("Ingrese la cantidad a depositar\n");
             scanf(" %i", &anadir);
             saldo = dinero_inicial + anadir;
             printf("Cantidad disponible en cuenta %i €\n", saldo);
             break;
          case 2:
             printf("Cuanto dinero desea retirar: \n");
             scanf(" %i", &reintegro);
                if (reintegro>1000){
                   printf("La cantidad que has puesto es mayor a mil dolares.\n");
                   printf("Introduzca de nuevo la cantidad correspondiente\n", reintegro);
                }
            saldo = dinero_inicial - reintegro;
            printf("La cantidad disponible en la cuenta es: %i €\n", saldo);
            break;
          case 3:
            printf("La cantidad disponible en cuenta es: %i €\n", dinero_inicial);
            break;
          case 4:
            printf("Gracias por confiar en nosotros\n");
            break;
        }
        if(menu>4){
          printf("Se ha equivocado de opcion\n");
        }
	return EXIT_SUCCESS;
}
